%bcond_with bootstrap

Name:           python-xmltodict
Version:        0.12.0
Release:        2
Summary:        Python module that makes working with XML feel like you are working with JSON 
License:        MIT
URL:            https://github.com/martinblech/xmltodict
Source0:        https://github.com/martinblech/xmltodict/archive/refs/tags/v0.12.0.tar.gz

BuildArch:      noarch

%description
Python module that makes working with XML feel like you are working with JSON

%package -n python3-xmltodict
Summary:        Python module that makes working with XML feel like you are working with JSON 
BuildRequires:       python3-pytest
BuildRequires:       python3-devel
BuildRequires:       python3-setuptools

%description -n python3-xmltodict
Python module that makes working with XML feel like you are working with JSON 

%prep
%autosetup -n xmltodict-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} -m pytest tests

%files -n python3-xmltodict
%doc README.md CHANGELOG.md
%license LICENSE
%{python3_sitelib}/*


%changelog
* Tue Aug 17 2021 wulei <wulei80@huawei.com> - 0.12.0-2
- add buildrequires

* Tue Apr 28 2020 Wei Xiong  <myeuler@163.com>
- init package
